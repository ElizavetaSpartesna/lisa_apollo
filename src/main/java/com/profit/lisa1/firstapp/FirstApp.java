package com.profit.lisa1.firstapp;

import static java.lang.System.*;


/**
 * Created by lisa on 27.05.17.
 */
public class FirstApp {

    //можно создать любой объект и его распечатать

    /**
     * print method
     * @param t
     * @param <T>
     */
    public static<T> void print(T t) {
    out.print(t);
    }

    /**
     * println method that prints objects
     * @param object
     */
    public static void  println(Object object) {
        out.println(object);
    }

    /**
     * println method that prints new line
     */
    public static void println() {
        out.println();
    }
}
