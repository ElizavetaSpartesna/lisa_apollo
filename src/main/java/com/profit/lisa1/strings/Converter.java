package com.profit.lisa1.strings;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * Created by lisa on 10.06.17.
 */
public class Converter {
    /**
     * @param xmlSource
     * @return
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public static Document stringToDom(String xmlSource) throws SAXException,
            ParserConfigurationException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }

    /**
     * @param doc
     * @return
     * @throws TransformerException
     */
    public String documentToString(org.w3c.dom.Document doc) throws TransformerException {

        //create dom source for the document
        DOMSource domSource = new DOMSource(doc);
        //create a tring writer
        StringWriter stringWriter = new StringWriter();
        //create the result stream for the trasform
        StreamResult result = new StreamResult(stringWriter);
        //create a Transfrormer to serialize the document
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        transformer.setOutputProperty("indent", "");
        //Transform the document to the result stream
        transformer.transform(domSource, result);
        return stringWriter.toString();
    }

    /**
     * @param filename
     * @return
     * @throws IOException
     */
    public static StringBuilder lineXml(String filename) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            String line;


            while ((line = br.readLine()) != null) {
                sb.append(line.trim());

            }
            return sb;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb;
    }
}
