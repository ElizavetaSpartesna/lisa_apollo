package com.profit.lisa1.strings;

import java.util.StringTokenizer;

/**
 * Created by lisa on 10.06.17.
 */
public class StringTokenizing {
    /**
     *
     * @param str
     */
    public static void splitBySpace(String str) {

        StringTokenizer st = new StringTokenizer(str);

        System.out.println("------Split by space------");
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     *
     * @param str
     */
    public static void splitByComma(String str) {
        System.out.println("--------Split by comma ',' ------");
        StringTokenizer stSecond = new StringTokenizer(str, ",", true);

        while (stSecond.hasMoreElements()) {
            System.out.println(stSecond.nextElement());
        }
    }
}
