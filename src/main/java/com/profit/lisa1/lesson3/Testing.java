package com.profit.lisa1.lesson3;

/**
 * Created by lisa on 27.05.17.
 */
public class Testing {

    protected Testing() {

    }

    /**
     * Prints "Testing"
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Testing :)");
    }
}
