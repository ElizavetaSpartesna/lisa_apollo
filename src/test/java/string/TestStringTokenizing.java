package string;

import org.junit.Test;
import com.profit.lisa1.strings.StringTokenizing;

/**
 * Created by lisa on 10.06.17.
 */
public class TestStringTokenizing {

    @Test
    public void testingStringSplitBySpace() {
        String str = "hedgfwe, wehdfwy hwew";
        StringTokenizing.splitBySpace(str);

    }

    @Test
    public void testingStringSplitByComma(){
        String str = "nwjsgfws, wefw, wef";
        StringTokenizing.splitByComma(str);
    }
}
