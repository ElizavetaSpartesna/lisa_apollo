package loopsandarrays;

import org.junit.Test;

import static com.profit.lisa1.loopsandarrays.Sorting.selectionSort;

/**
 * Created by lisa on 04.06.17.
 */
public class TestSorting {

    @Test
    public void TestSortingOne() {
        int [] array = {2,3,5,8};
        selectionSort(array);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }



    }

}
